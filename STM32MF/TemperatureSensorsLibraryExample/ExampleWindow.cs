using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.Input;
using Microsoft.SPOT.Presentation;
using Microsoft.SPOT.Presentation.Controls;
using Microsoft.SPOT.Presentation.Media;

using STM32F429I_Discovery.Netmf.Hardware;
using TemperatureSensorsLibrary.Commons;
using TemperatureSensorsLibrary.Sensors;

namespace TemperatureSensorsLibraryExample
{
    /// <summary>
    /// Window class for example application
    /// </summary>
    class ExampleWindow : Window
    {

        #region Temperature sensors objects

        private TemperatureSensor[] sensors = new TemperatureSensor[3];

        #endregion Temperature sensors objects

        private DispatcherTimer timer;

        private double LM35Temperature;
        private double DS18B20Temperature;
        private double TMP120Temperature;

        TemperatureFormat format;

        Font standardFont;
        Pen standardPen;
        Brush backgroundBrush;

        #region Public methods

        /// <summary>
        /// Public constuctor
        /// </summary>
        public ExampleWindow()
        {
            sensors[0] = new LM35DZ(ADC.Channel1_PA7, 3.3, 0, 12);
            sensors[1] = new DS18B20(new Microsoft.SPOT.Hardware.OutputPort(Cpu.Pin.GPIO_Pin2, false));
            sensors[2] = new TMP102();

            format = TemperatureFormat.CELSIUS;
            standardPen = new Pen(Color.White);
            backgroundBrush = new SolidColorBrush(Color.White);
            standardFont = Resources.GetFont(Resources.FontResources.small);
            
            timer = new DispatcherTimer(this.Dispatcher);
            timer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            timer.Tick += new EventHandler(TimerTick);
            timer.Start();
        }

        #endregion Public methods

        #region Event Hanlers

        /// <summary>
        /// Render event handler
        /// </summary>
        /// <param name="dc"></param>
        public override void OnRender(DrawingContext dc)
        {
            dc.DrawRectangle(backgroundBrush, standardPen, 0, 0, 240, 320);
            dc.DrawText("LM35: " + LM35Temperature.ToString("f2") +
                ((format == TemperatureFormat.CELSIUS) ? " Celsius" : " Kelvin"),
                standardFont, Color.Black, 15, 60);
            dc.DrawText("DS18B20: " + DS18B20Temperature.ToString("f2") +
                ((format == TemperatureFormat.CELSIUS) ? " Celsius" : " Kelvin"),
                standardFont, Color.Black, 15, 80);
            dc.DrawText("TMP120T: " + TMP120Temperature.ToString("f2") +
                ((format == TemperatureFormat.CELSIUS) ? " Celsius" : " Kelvin"),
                standardFont, Color.Black, 15, 100);
        }

        /// <summary>
        /// Touch event handler
        /// </summary>
        /// <param name="e"></param>
        protected override void OnTouchUp(TouchEventArgs e)
        {
            format = TemperatureFormat.KELVIN == format ? TemperatureFormat.CELSIUS :
                TemperatureFormat.KELVIN;
            e.Handled = true;
        }

        /// <summary>
        /// Timer event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimerTick(object sender, EventArgs e)
        {
 	        LM35Temperature = sensors[0].ReadTemperature(format);
            DS18B20Temperature = sensors[1].ReadTemperature(format);
            TMP120Temperature = sensors[2].ReadTemperature(format);
            Invalidate();
        }

        #endregion Event Hanlers
    }
}
